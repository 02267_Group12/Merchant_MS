package dtu.clients;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class ReportClient {

    private static String getRootPath(){
        String env = System.getenv("RUNNING_ENV");
        if (env == null) return "http://localhost:";
        else return "http://reportgeneration:";
    }


    static String port = "5007/";
    static WebTarget baseUrl = ClientBuilder.newClient().target(getRootPath() + port);
    public Response getMerchantReport(String mid){
        return baseUrl
                .path("report")
                .path("merchant")
                .path(mid)
                .request()
                .get();
    }


}
