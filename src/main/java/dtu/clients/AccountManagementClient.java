/*
package dtu.clients;

import dtu.rs.entities.DTUPayAccount;
import dtu.ws.fastmoney.User;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class AccountManagementClient {

    static String port = "5003/";
    static WebTarget baseUrl = ClientBuilder.newClient().target("http://localhost:" + port);

    public Response registerNewCustomer(User user){
        return baseUrl
                .path("accounts")
                .path("customer")
                .request()
                .post(Entity.entity(user, MediaType.APPLICATION_JSON));
    }

    public Response retrieveCustomerAccount(String cid){
        Response response = baseUrl
                .path("accounts")
                .path("customer")
                .path(cid)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
        return response;
    }

}*/

package dtu.clients;

import dtu.rs.entities.DTUPayAccount;
import dtu.ws.fastmoney.User;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class AccountManagementClient {

    WebTarget baseUrl;


    private static String getRootPath(){
        String env = System.getenv("RUNNING_ENV");
        if (env == null) return "http://localhost:";
        else return "http://accountmanagement:";
    }

    public AccountManagementClient() {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target(getRootPath() + "5002/");
    }

    public Response registerNewMerchant(User user){
        return baseUrl
                .path("accounts")
                .path("merchant")
                .request()
                .post(Entity.entity(user, MediaType.APPLICATION_JSON));
    }

    public Response retrieveMerchantAccount(String id){
        return baseUrl
                .path("accounts")
                .path("merchant")
                .path(id)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
    }




}


