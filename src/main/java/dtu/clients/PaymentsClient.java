package dtu.clients;

import dtu.rs.entities.MerchantPaymentData;
import dtu.rs.entities.MerchantTransaction;

import javax.print.attribute.standard.Media;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class PaymentsClient {

    private static String getRootPath(){
        String env = System.getenv("RUNNING_ENV");
        if (env == null) return "http://localhost:";
        else return "http://payment:";
    }

    static String port = "5001/";
    static WebTarget baseUrl = ClientBuilder.newClient().target(getRootPath() + port);
    public Response payToMerchant(String transactionId, String tid){
        return baseUrl
                .path("payments")
                .path(transactionId)
                .request()
                .post(Entity.entity(tid, MediaType.TEXT_PLAIN));
    }

    public Response initiateTransaction(MerchantTransaction transaction){
        return baseUrl
                .path("payments")
                .request()
                .post(Entity.entity(transaction, MediaType.APPLICATION_JSON));
    }
}
