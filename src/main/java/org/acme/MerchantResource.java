package org.acme;

import dtu.clients.AccountManagementClient;
import dtu.clients.PaymentsClient;
import dtu.clients.ReportClient;
import dtu.rs.entities.*;
import dtu.ws.fastmoney.Transaction;
import dtu.ws.fastmoney.User;
import entity.local.DTUTransaction;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Path("/merchant")
public class MerchantResource {

    AccountManagementClient accountsClient = new AccountManagementClient();
    PaymentsClient paymentsClient = new PaymentsClient();
    ReportClient reportClient = new ReportClient();
    static String baseUrl = "http://localhost:";
    static String port = "8080";
    List<Token> activeTokens = new ArrayList<Token>();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registerNewMerchant(User user){
        Response response = accountsClient.registerNewMerchant(user);
        if (response.getStatus() == 200){
            String cid = response.readEntity(String.class);
            return Response
                    .ok()
                    .location(URI.create(baseUrl + port + "/merchant/" + cid))
                    .build();
        } else {
            return Response
                    .status(response.getStatus(), "Could not register user with cpr number: " + user.getCprNumber())
                    .entity("Could not register user with cpr number: " + user.getCprNumber())
                    .build();
        }
    }



    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/payment")
    public Response payToMerchant(MerchantTransaction merchantTransaction){
        Response response = paymentsClient.initiateTransaction(merchantTransaction);
        if (response.getStatus() == 200){
            String transactionId = response.readEntity(String.class);
            response = paymentsClient.payToMerchant(transactionId, merchantTransaction.getTokenId());

            while(response.getStatus() == 425){
                response = paymentsClient.payToMerchant(transactionId, merchantTransaction.getTokenId());
                System.out.println(response.getStatusInfo());
            }
        }
        return response;
    }


    @GET
    @Path("/{mid}/report")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMerchantReport(@PathParam("mid") String mid){
        Response response = reportClient.getMerchantReport(mid);
        if (response.getStatus() == 200){
            return response;
        } else {
            return Response
                    .status(response.getStatus(), "Failed to get all transactions from: " + mid)
                    .entity("Failed to get all transactions from: " + mid)
                    .build();
        }
    }


    @GET
    @Path("/{mid}")
    public Response getMerchantById(@PathParam("mid") String mid)
    {
        Response response = accountsClient.retrieveMerchantAccount(mid);
        if (response.getStatus() == 200) {
            DTUPayAccount account = response.readEntity(new GenericType<DTUPayAccount>() {});
            return Response
                    .ok(new GenericEntity<DTUPayAccount>(account){})
                    .build();
        } else {
            return Response
                    .status(response.getStatus(), "Failed to get merchant with mid: " + mid)
                    .entity("Failed to get merchant with mid: " + mid)
                    .build();
        }
    }



}
